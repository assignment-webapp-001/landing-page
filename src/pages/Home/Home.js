import React, {Component} from 'react';
import styled from "styled-components";
import {DetailSection, Footer, HeroSection, Nav, SubDetailSection, SubHeroSection} from "../../components";
import '../../Typography.css'

const Div = styled.div`
  position: relative;
  width: 100%;
  float: left;
`;

class Home extends Component {
    render() {
        return (<Div>
            <Nav/>
            <HeroSection/>
            <SubHeroSection/>
            <DetailSection/>
            <SubDetailSection/>
            <Footer/>
        </Div>);
    }
}

Home.propTypes = {};

export default Home;
