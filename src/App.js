import './App.css';
import {Home} from "./pages";
import {Fragment} from "react";

function App() {
    return (
        <Fragment>
            <Home/>
        </Fragment>
    );
}

export default App;
