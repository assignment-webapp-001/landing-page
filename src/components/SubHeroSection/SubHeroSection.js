import React, {Component} from 'react';
import styled from "styled-components";
import PropTypes from 'prop-types';

const Div = styled.div`
  position: relative;
  width: 100%;
  background: #fafafa;

  .box {
    max-width: 1024px;
    margin: 0 auto;
    padding: 60px 20px;

    .title {
      text-align: center;
      padding-bottom: 20px;
      position: relative;
      width: 100%;
    }

    .sub-title {
      position: relative;
      width: 100%;
      font-weight: normal;
      color: #555a65;
      max-width: 640px;
      margin: 0 auto;
      text-align: center;
      padding-bottom: 20px;
    }

    .wrap-card {
      position: relative;
      width: 100%;
      display: flex;
      flex-wrap: wrap;
      flex-direction: row;

      .card {
        flex: 1 0 25%;

        .card-title {
          font-weight: bold;
          padding: 0 20px 10px;
        }

        .card-sub-title {
          padding: 0 20px;
          color: #555a65;
        }

        img {
          max-width: 100%;
        }
      }
    }
  }

  @media screen and (max-width: 992px) {
    .box {
      .wrap-card {
        .card {
          flex: 1 0 50%;
        }
      }
    }
  }
  @media screen and (max-width: 600px) {
    .box {
      .wrap-card {
        .card {
          flex: 1 0 100%;
        }
      }
    }
  }
`;

class SubHeroSection extends Component {
    static defaultProps = {
        cards: [{
            image: '/assets/images/how-it-work-1.png',
            title: '1. Lorem insum oder',
            detail: 'Lorem ipsum dolor sit amet.'
        }, {
            image: '/assets/images/how-it-work-2.png',
            title: '2. Odee lorem niosum',
            detail: 'Velit risus semper proin ac, aliquam tristique justo tristique justo.'
        }, {
            image: '/assets/images/how-it-work-3.png',
            title: '3. Yopus orem ipsum ode',
            detail: 'Maoronac ultricioc amet justo, gstas enm tmou ultricioc amet justo.'
        }, {
            image: '/assets/images/how-it-work-4.png',
            title: '4. Mom ipsum lorem',
            detail: 'Lorem ipsum dolor sit amet,consectetur adolscing ell.'
        }]
    }

    render() {
        return (<Div>
            <div className='box'>
                <div className='h2 title'>How BetterGoods works?</div>
                <div className='h4 sub-title'>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed quis tellus ante. Morbi ac nunc eu
                    metus interdum efficitur sit amet at mi. Etiam mattis risus eu sollicitudin varius.
                </div>
                <div className='wrap-card'>
                    {this.props && this.props.cards.map((item, key) => (<div className='card' key={key}>
                        <img src={item.image} width='100%'/>
                        <div className='p1 card-title'>{item.title}</div>
                        <div className='p1 card-sub-title'>{item.detail}</div>
                    </div>))}
                </div>
            </div>
        </Div>);
    }
}

SubHeroSection.propTypes = {
    cards: PropTypes.array
};

export default SubHeroSection;
