import React, {Component} from 'react';
import styled from "styled-components";

const Div = styled.div`
  position: sticky;
  width: 100%;
  margin: 0 auto;
  bottom: 0;
  background: #fff;
  z-index: 500;

  .box {
    max-width: 1024px;
    margin: 0 auto;
    padding: 20px;

    .row {
      position: relative;
      width: 100%;
      display: flex;
      justify-content: space-between;
      align-items: center;

      .left {
        display: flex;

        .menu {
          padding-left: 80px;
          display: flex;
          justify-content: space-between;
          align-items: center;

          .item {
            text-decoration: none;
            color: #555a65;
            cursor: pointer;
            padding: 0 10px;
          }
        }
      }

      .social {
        display: flex;

        .icon {
          text-decoration: none;
          color: #555a65;
          cursor: pointer;
          padding: 0 10px;

          &:last-child {
            padding-right: 0;
          }
        }
      }
    }
  }

  @media screen and (max-width: 768px) {
    .box {
      .row {
        flex-wrap: wrap;
        flex-direction: row;

        .left {
          flex: 1 0 100%;
          flex-wrap: wrap;
          flex-direction: row;

          .menu {
            flex-wrap: wrap;
            flex-direction: row;
            padding-left: 0;
            flex: 1 0 100%;

            .item {
              padding: 10px;
              flex: 1 0 100%;
            }
          }
        }

        .social {
          padding-top: 20px;
          flex: 1 0 100%;
        }
      }
    }
  }
`;

class Footer extends Component {
    render() {
        return (<Div>
            <div className='box'>
                <div className='row'>
                    <div className='left'>
                        <div><img src='/assets/icons/logo-alone.png' height='30px'/></div>
                        <div className='menu'>
                            <a className='item' href='#'>Menu1</a>
                            <a className='item' href='#'>Menu2</a>
                            <a className='item' href='#'>Menu3</a>
                        </div>
                    </div>
                    <div className='social'>
                        <a className='icon' href='#'><img src='/assets/icons/facebook-logo.png' height='30px'/></a>
                        <a className='icon' href='#'><img src='/assets/icons/twitter-logo.png' height='30px'/></a>
                        <a className='icon' href='#'><img src='/assets/icons/instagram-logo.png' height='30px'/></a>
                        <a className='icon' href='#'><img src='/assets/icons/youtube-logo.png' height='30px'/></a>
                    </div>
                </div>
            </div>
        </Div>);
    }
}

Footer.propTypes = {};

export default Footer;
