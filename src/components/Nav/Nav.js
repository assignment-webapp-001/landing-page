import React, {Component} from 'react';
import styled from 'styled-components';

const Div = styled.div`
  position: sticky;
  width: 100%;
  margin: 0 auto;
  top: 0;
  background: #fff;
  z-index: 500;

  .nav {
    max-width: 1024px;
    margin: 0 auto;
    padding: 20px;

    .row {
      position: relative;
      width: 100%;
      display: flex;
      justify-content: space-between;
      align-items: center;

      .nav-left {
        .nav-btn {
          display: none;
        }

        #nav-check {
          display: none;
        }
      }

      .menu {
        display: flex;
        justify-content: space-between;
        align-items: center;

        .item {
          text-decoration: none;
          color: #000;
          cursor: pointer;
          padding: 0 20px;
        }
      }
      
      .nav-links {
        display: none;
      }

      .right {
        display: flex;
        justify-content: space-between;
        align-items: center;

        .logout {
          padding: 0 20px;
          text-decoration: none;
          color: #000;
          cursor: pointer;
        }

        .profile {
          display: flex;
          justify-content: space-between;
          align-items: center;

          .arrow {
            padding-left: 10px;
          }
        }
      }
    }
  }

  @media screen and (max-width: 768px) {
    .nav {
      .row {
        justify-content: normal;

        .nav-left {
          .nav-btn {
            display: inline-block;
            position: absolute;
            right: 0;
            top: 0;

            label {
              padding-top: 5px;
              display: inline-block;

              span {
                display: block;
                width: 25px;
                height: 10px;
                border-top: 2px solid #56cbef;
              }
            }
          }
        }

        .menu, .right {
          display: none;
        }

        .nav-links {
          display: initial;
          position: fixed;
          width: 100%;
          background-color: #fff;
          transition: all 0.3s ease-in;
          overflow-y: hidden;
          height: 0;
          top: 60px;
          left: 0px;

          a {
            color: #000;
            text-align: center;
            display: block;
            width: 100%;
            padding: 10px;
          }

          .profile {
            display: flex;
            align-items: center;
            justify-content: center;
            padding-bottom: 60px;

            .arrow {
              padding-left: 10px;
            }
          }
        }
      }
    }

    .nav > .row > .nav-left > #nav-check:not(:checked) ~ .nav-links {
      height: 0px;
    }

    .nav > .row > .nav-left > #nav-check:checked ~ .nav-links {
      height: 260px;
      //overflow-y: auto;
    }
  }
`;

class Nav extends Component {
    render() {
        return (<Div>
            <div className='nav'>
                <div className='row'>
                    <div className='nav-left'>
                        <input type="checkbox" id="nav-check"/>
                        <div><img src='/assets/icons/logo-full.png' height='30px'/></div>
                        <div className="nav-btn">
                            <label htmlFor="nav-check">
                                <span></span>
                                <span></span>
                                <span></span>
                            </label>
                        </div>
                        <div className='nav-links'>
                            <a className='item' href='#'>Explore products</a>
                            <a className='item' href='#'>Product design</a>
                            <a className='item' href='#'>Pricing</a>
                            <a className='logout' href='#'>Logout</a>
                            <a className='profile' href='#'>
                                <div><img src='/assets/icons/profile.png' height='30px'/></div>
                                <div className='arrow'><img src='/assets/icons/32195.png' height='10px'/></div>
                            </a>
                        </div>
                    </div>
                    <div className='menu'>
                        <a className='item' href='#'>Explore products</a>
                        <a className='item' href='#'>Product design</a>
                        <a className='item' href='#'>Pricing</a>
                    </div>
                    <div className='right'>
                        <a className='logout' href='#'>Logout</a>
                        <a className='profile' href='#'>
                            <div><img src='/assets/icons/profile.png' height='30px'/></div>
                            <div className='arrow'><img src='/assets/icons/32195.png' height='10px'/></div>
                        </a>
                    </div>
                </div>
            </div>
        </Div>);
    }
}

Nav.propTypes = {};

export default Nav;
