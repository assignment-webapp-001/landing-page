export {Nav} from './Nav';
export {Footer} from './Footer';
export {HeroSection} from "./HeroSection";
export {SubHeroSection} from "./SubHeroSection";
export {DetailSection} from "./DetailSection";
export {SubDetailSection} from "./SubDetailSection";


