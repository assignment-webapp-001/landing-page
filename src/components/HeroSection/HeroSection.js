import React, {Component} from 'react';
import styled from "styled-components";
import '../../Typography.css'

const Div = styled.div`
  position: relative;
  width: 100%;
  max-width: 1024px;
  margin: 0 auto;

  .content {
    position: relative;
    width: 100%;
    display: flex;
    align-items: center;
    padding: 60px 20px;

    .text {
      position: relative;
      width: 100%;

      .title {
        padding-bottom: 20px;
      }

      .sub-title {
        font-weight: normal;
        color: #777f96;
      }
    }

    .image {
      position: relative;
      width: 100%;
      max-width: 500px;

      img {
        object-fit: contain;
      }
    }
  }

  @media screen and (max-width: 768px) {
    .content {
      flex-wrap: wrap;
      flex-direction: row;

      .text {
        flex: 1 0 100%;
        order: 2;
      }

      .image {
        flex: 1 0 100%;
        order: 1;
        padding-bottom: 30px;
        max-width: 100%;
      }
    }
  }
`;

class HeroSection extends Component {
    render() {
        return (
            <Div>
                <div className='content'>
                    <div className='text'>
                        <div className='h1 title'>
                            Higher click-through
                            rates for your product
                        </div>
                        <div className='h4 sub-title'>
                            Don't stop tracking your product and better analyze your customers just too easy steps.
                        </div>
                    </div>
                    <div className='image'>
                        <img src='/assets/images/click-through-rate.png' width='100%'/>
                    </div>
                </div>
            </Div>
        );
    }
}

HeroSection.propTypes = {};

export default HeroSection;
