import React, {Component} from 'react';
import styled from "styled-components";

const Div = styled.div`
  position: relative;
  width: 100%;
  background-image: url('/assets/images/power-up-1.png');
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  color: #fff;

  .box {
    max-width: 1024px;
    margin: 0 auto;
    padding: 60px 20px;
    text-align: center;

    .text-padding {
      padding-bottom: 40px;
    }
  }
`;

class DetailSection extends Component {
    render() {
        return (
            <Div>
                <div className='box'>
                    <div className='h2 text-padding'>Power-up your performance</div>
                    <div className='p1 text-padding'>Augue pellentesque edestas iosum justo. dictum commodo in.erat a
                        Vel: bibendum consect etur
                        convallis faucibus velit elementum lacus ac Rh
                        arcu commodo enim. genean edet at tellus. Blandit viverra nascetur amet tr
                        aue nunc laoreet Massa elit semper
                        eleifend tristique tristique Tempor eu euismod tincidunt vestibulum nu imperdiet gravida
                        facilisi,eleifend tristique tristique Tempor eu euismod tincidunt vestibulum nu imperdiet
                        gravida morbi.
                    </div>
                    <div className='p1'>Augue pellentesque edestas iosum justo. dictum commodo in.erat a Vel: bibendum
                        consect etur
                        convallis faucibus velit elementum lacus ac Rh
                        arcu commodo enim. genean edet at tellus. Blandit viverra nascetur amet tr
                        aue nunc laoreet Massa elit semper
                        eleifend tristique tristique Tempor eu euismod tincidunt vestibulum nu imperdiet gravida
                        facilisi,eleifend tristique tristique Tempor eu euismod tincidunt vestibulum nu imperdiet
                        facilisi, morbi.
                    </div>
                </div>
            </Div>
        );
    }
}

DetailSection.propTypes = {};

export default DetailSection;
