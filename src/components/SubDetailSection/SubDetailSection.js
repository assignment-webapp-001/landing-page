import React, {Component} from 'react';
import styled from "styled-components";
import PropTypes from "prop-types";

const Div = styled.div`
  position: relative;
  width: 100%;
  background: #fafafa;

  .box {
    max-width: 1024px;
    margin: 0 auto;
    padding: 60px 20px;

    .wrap-card {
      position: relative;
      width: 100%;
      display: flex;
      justify-content: space-between;

      .card {
        text-align: center;
        margin: 5px;

        .card-title {
          font-weight: bold;
          padding: 20px 0;
        }

        .card-sub-title {
          color: #555a65;
        }

        img {
          max-width: 100%;
        }
      }
    }
  }

  @media screen and (max-width: 600px) {
    .box {
      max-width: 100%;
      padding: 0;

      .wrap-card {
        flex-wrap: wrap;
        flex-direction: row;

        .card {
          flex: 1 0 100%;
          padding-bottom: 60px;
          margin: 0;
        }
      }
    }
  }
`;

class SubDetailSection extends Component {
    static defaultProps = {
        cards: [{
            image: '/assets/images/power-up-2.png',
            title: 'Yopus orem ipsum oder',
            detail: 'Lorem ipsum dolor sit amet, Purus ipsum est facilisi mi sed nec ultrices.'
        }, {
            image: '/assets/images/power-up-3.png',
            title: 'Odee lorem ipsum',
            detail: 'Lorem ipsum dolor sit amet, Purus ipsum est facilisi mi sed nec ultrices.'
        }]
    }

    render() {
        return (<Div>
            <div className='box'>
                <div className='wrap-card'>
                    {this.props && this.props.cards.map((item, key) => (<div className='card' key={key}>
                        <img src={item.image}/>
                        <div className='h4 card-title'>{item.title}</div>
                        <div className='p1 card-sub-title'>{item.detail}</div>
                    </div>))}
                </div>
            </div>
        </Div>);
    }
}

SubDetailSection.propTypes = {
    cards: PropTypes.array
};

export default SubDetailSection;
